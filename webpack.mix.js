const { mix } = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

// production has trouble with uglify right now
mix.options({
   uglify:false
});

mix.webpackConfig({
    watchOptions: {
      poll: 200
    },
});

mix.copy('resources/assets/images', 'public/img', false);

mix.js('resources/assets/js/app.js', 'public/js')
    .js('resources/assets/js/admincustomer.js', 'public/js')
    .js('resources/assets/js/adminuser.js', 'public/js')
    .js('resources/assets/js/adminrole.js', 'public/js')
    .js('resources/assets/js/admingroup.js', 'public/js')
.sass('resources/assets/sass/front.scss', 'public/css');
