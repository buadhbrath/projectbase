<?php

namespace Tests\Unit;

use App\Services\RoleService;
use App\Services\UserService;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class UserTest extends TestCase
{

    use DatabaseTransactions;

    public function testUser()
    {
        $u = $this->createTestUser(2);
        $this->assertTrue($u->name == "user 2");
    }

    public function testUserRole()
    {
        $u = $this->createTestUser(23);
        $r = RoleService::getByIdentifier("ROLE_USER");
        UserService::setRoles($u, [$r]);
        $u2    = UserService::getById($u->id);
        $roles = $u2->roles();
        $this->assertTrue(count($roles) == 1);

    }

    /**
     * @param int $num
     * @return \App\User
     */
    public function createTestUser($num = 1)
    {
        $a             = [];
        $a['name']     = "user $num";
        $a['email']    = "user$num@kimsal.com";
        $a['password'] = "pass$num";

        $u = UserService::create($a);

        return $u;
    }
}
