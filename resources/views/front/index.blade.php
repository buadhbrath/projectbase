@extends('layouts.front')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-6" id="fronthero">
            <h1>{{ config("adminlte.title") }}</h1>
            <h2>kick start your next PHP project!</h2>
            <ul>
                <li>best of breed components</li>
                <li>built with laravel, bootstrap and other great tools</li>
                <li>user registration, groups and roles management</li>
                <li>ships with vagrant setup, unit tests and more!</li>
            </ul>
        </div>
        <div class="col-md-6">
            <img id="deviceimage" class="img-responsive " src="{{ url("img/devices.png") }}" alt="image of devices"/>
        </div>

    </div>
    <div class="row">
        <div class="col-md-12  text-center">
            <br/>
            <br/>
            <p>
            <a href="https://bitbucket.org/kimsal/projectbase" class="center btn btn-lg btn-success">Grab the
        code!</a>
            </p>
        </div>
    </div>
</div>
@endsection
