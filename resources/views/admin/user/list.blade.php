@extends("adminlte::page")

@section("content_header")
    <div class="container">
        <div class="pull-left">
            <div class="bigfont" style="font-size:1.5em; font-weight:bold;">Users</div>
        </div>
        <div class="pull-right" style="margin-right:5em">
            <a class="btn btn-info" href="{{ url("#/new") }}">New</a>
            <a class="btn btn-info" href="{{ url("#/") }}">List</a>
        </div>
    </div>
@endsection

@section('content')
    <div id="useradmintable">
        <router-view></router-view>
    </div>
@endsection

@section('page_js')
    <script src="{{ asset('/js/adminuser.js') }}"></script>
@endsection
