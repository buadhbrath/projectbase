
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

Vue = require('vue');
var VueResource = require('vue-resource');
Vue.use(VueResource);


import adminroletable from './components/AdminRoleTable.vue';
import adminroleedit from './components/AdminRoleEdit.vue';
import adminrolenew from './components/AdminRoleNew.vue';
import VueRouter from 'vue-router';

const routes = [
  { path: '/', component: adminroletable,name:"adminroletable" },
  { path: '/edit/:id', component: adminroleedit, name:"adminroleedit" },
  { path: '/new', component: adminrolenew, name:"adminrolenew" },
];
const router = new VueRouter({
  routes:routes // short for routes: routes
});


window.ua = new Vue({
    router:router,
    el: '#adminroletable',
    components: {
        adminroletable: adminroletable
    },
    data: {
        table_columns: [
            {label: 'Name', field: 'name'},
            {label: 'Identifier', field: 'identifier'},
        ],
        table_rows: []
    },
    mounted() {}
});

$(function(){
    $("[data-hide]").on("click", function(){
        $("." + $(this).attr("data-hide")).hide();
    });
});
