
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

Vue = require('vue');
var VueResource = require('vue-resource');
Vue.use(VueResource);


import admincustomertable from './components/AdminCustomerTable.vue';
import admincustomeredit from './components/AdminCustomerEdit.vue';
import admincustomernew from './components/AdminCustomerNew.vue';
import VueRouter from 'vue-router';

const routes = [
  { path: '/', component: admincustomertable,name:"admincustomertable" },
  { path: '/edit/:id', component: admincustomeredit, name:"admincustomeredit" },
  { path: '/new', component: admincustomernew, name:"admincustomernew" },
];
const router = new VueRouter({
  routes:routes // short for routes: routes
});


window.ua = new Vue({
    router:router,
    el: '#admincustomertable',
    components: {
        admincustomertable: admincustomertable
    }});

$(function(){
    $("[data-hide]").on("click", function(){
        $("." + $(this).attr("data-hide")).hide();
    });
});
