<?php

use App\Group;
use App\Role;
use App\Team;
use App\User;
use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [];
        $groups = [];
        $roles =  [];
        $teams = [];


        $u = new User();
        $u->name = "user0";
        $u->email = "user0@k.com";
        $u->password = bcrypt("pass");
        $u->save();
        $users['user0'] = User::where('name',"user0")->first();

        $x = 1;
        while($x<=5)
        {
            DB::table('user')->insert([
                'name' => "user$x",
                'email' => "user$x@k.com",
                'api_token'=>"token$x",
                'password' => bcrypt('pass'),
            ]);
            $users['user'.$x] = User::where('name',"user$x")->first();
            $x++;
        }

        $x=1;
        while($x<=5)
        {
            DB::table('group')->insert([
                'name' => "group $x",
                'identifier' => "g$x",
            ]);
            $groups['group'.$x] = Group::where('identifier',"g$x")->first();
            $x++;
        }

        $x=1;
        while($x<=5)
        {
            DB::table('team')->insert([
                'name' => "team $x",
                'identifier' => "t$x",
            ]);
            $teams['team'.$x] = Team::where('identifier',"t$x")->first();
            $x++;
        }

        $r = ['ROLE_USER','ROLE_ADMIN','ROLE_SWITCH_USER','ROLE_SUPER_ADMIN'];
        foreach($r as $k)
        {
            $n = strtolower(str_replace("ROLE_","",$k));
            DB::table('role')->insert([
               'name'=>$n,
               'identifier'=>$k
            ]);
            $roles[strtolower($k)] = Role::where('identifier',$k)->first();
        }

        /** @var User $u */
        $u = $users['user0'];
        $u->groups()->saveMany([$groups['group1'],$groups['group2']]);
        $u->roles()->saveMany([$roles['role_admin'], $roles['role_super_admin'], $roles['role_switch_user']]);
        $u->teams()->save($teams['team1']);


    }
}
