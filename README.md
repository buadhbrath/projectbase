# Getting started... #


May ask for a password when cloning - hit enter with an empty password...
```
#!bash
> git clone https://kimsal@bitbucket.org/kimsal/projectbase.git
> cd projectbase
```

# To set up virtual machine #

You'll need 'vagrant' (at least version 1.8.6, only tested on 1.9.x) and 'virtualbox' (min 5.1) already set up on your main system.

You'll need to unpack the puppet support files first:

```
#!bash
tar -xzf puphpet_files.tgz
```

Then...

```
#!bash
> vagrant up
```

The first time this runs, it will

* install system dependencies
* install npm dependencies
* install php dependencies
* set up your .env laravel file
* run the first database migration and seed with sample data
* run the npm build process once to compile the CSS/JS

Finally, you'll need to edit /etc/hosts on your local host machine to point to 192.168.10.10

```
#!bash
192.168.10.10 projectbase.dev
```

You should be able to visit 'http://projectbase.dev' and see the initial screen.

## Other stuff ##

When you
```
#!bash
> vagrant ssh
```

you'll be taken to /home/vagrant, then you'll need to cd to /var/www

```
#!bash
> cd /var/www
```

To have the system watch your css and js files, and auto-compile them,
you'll need to be in the virtual machine, in /var/www, then run

```
#!bash
> npm run watch
```



# To set up without virtual machine #

If you're going to use this directly, without a virtual machine, you'll need to have
modern/current setup of MySQL (or similar), PHP7 and npm.

```
#!bash

> composer install
> npm install
> cp .env.example .env  (see notes below)
> php artisan key:generate
> npm run dev
```

## Notes ##

The .env file by default points to mysql 'projectbase', and if using the virtual machine,
things will work with these defaults.  If using a different/local instance, modify the .env
file appropriately.

The default 'title' which will show up in the page title in the default view
templates is in the .env file - modify as desired.