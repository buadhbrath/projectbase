<?php

namespace App;

use App\Traits\Validatable;
use Illuminate\Database\Eloquent\Model;

class Note extends Model
{
    use Validatable;

    const TYPE_USER = "user";
    const TYPE_SYSTEM = "system";
    const VISIBILITY_PRIVATE = "private";
    const VISIBILITY_PUBLIC = "public";
    const VISIBILITY_ALL = "all";

    protected $table = "note";
    
}
