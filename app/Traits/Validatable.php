<?php

namespace App\Traits;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Validator;
use Ramsey\Uuid\Uuid;

trait Validatable
{

    public $_errors = [];



    protected $validationRules = [
//           'email' => 'required|email',
//           'name'  => 'required',
    ];


    public function validate($data)
    {
        if(count($this->validationRules)==0)
        {
            return true;
        }
        // make a new validator object
        $v = Validator::make($data, $this->validationRules);

        // return the result
        // check for failure
        if ($v->fails())
        {
            // set errors and return false

            $this->_errors = $v->getMessageBag();

            return false;
        }
        return true;
    }

    public function errors()
    {
        $t = [];
        if(!is_object($this->_errors))
        {
            return $t;
        }
        foreach($this->_errors->getMessages() as $m)
        {
            $t[] = $m[0];
        }
        return $t;
    }


}
