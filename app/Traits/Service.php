<?php
/**
 * Created by michael.
 * Date: 5/9/17 Time: 2:33 PM
 */

namespace App\Traits;


use App\User;
use Illuminate\Support\Facades\Hash;

trait Service
{

    /**
     * we might have an array or we might have an eloquent/db object
     * (or something with 'toArray' on it)
     * return an array in either care
     * @param $x
     * @return array
     */
    static public function convertToArray($x)
    {
        $array = [];
        if(is_object($x) && method_exists($x, "toArray"))
        {
            $array = $x->toArray();
        } else {
            $array = $x;
        }
        return $array;
    }

    /**
     * @param $object
     * @param $data
     * @throws \Exception
     */
    static public function validateAndFill($object, $data)
    {
        $d = self::convertToArray($data);

        if(is_object($object)) {
            if(method_exists($object, "validate")) {
                if (!$object->validate($d)) {
                    throw new \Exception(get_class($object) . " did not validate");
                }
            }
            if(method_exists($object, "fill")) {
                $object->fill($d);
            }
        }
    }


}