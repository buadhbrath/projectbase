<?php

namespace App;

use App\Traits\Validatable;
use Illuminate\Database\Eloquent\Model;

class Address extends Model
{

    const ADDRESS_TYPE_PHYSICAL = "Physical";
    const ADDRESS_TYPE_MAILING = "Mailing";
    const ADDRESS_TYPE_OTHER = "Legal";

    use Validatable;

    protected $table = "address";

    protected $fillable = ['address1','address2',
                           'city','state','postal_code',
                           'sort_order',
                           'note','is_primary','type'];
}
