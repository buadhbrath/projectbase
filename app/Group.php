<?php

namespace App;

use App\Traits\Validatable;
use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    use Validatable;

    public $table = "group";

    public $_errors = [];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'identifier'
    ];


    public function users()
    {
        return $this->belongsToMany('App\User');
    }




}
