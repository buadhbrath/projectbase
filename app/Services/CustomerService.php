<?php

namespace App\Services;

use App\Customer;
use App\Note;
use App\Traits\Service;

class CustomerService
{
    use Service;

    /**
     * @param array $data
     * @return Customer
     */
    static public function create(array $data)
    {
        $c = new Customer();
        if($c->validate($data)) {
            $c->fill($data);
        }
        $c->save();
        return $c;
    }

    /**
     * @param Customer $customer
     */
    static public function delete(Customer $customer)
    {
        $customer->delete();
    }

    /**
     * @param $id
     * @return Customer
     */
    static public function getById($id)
    {
        $c = Customer::find($id);
        return $c;
    }


    static public function getAll()
    {
        $c = Customer::all();
        return $c;
    }

    static public function getAllWithPrimaryAddress()
    {

        $customers = Customer::leftJoin('address', function($join) {
              $join->on('customer.id', '=', 'address.customer_id');
            })
            ->where('address.is_primary',true)
            ->select(['customer.id','customer.first_name','customer.last_name',
                'address.address1','address.address2','address.city','address.state','address.postal_code'
                ])
            ->get();
        return $customers;
    }


    static public function getAddresses(Customer $customer)
    {
        $a = $customer->addresses()->orderBy("sort_order")->get();
        return $a;
    }


    /**
     * @param Customer $customer
     * @param string $publicPrivate public|private|all
     * @param string $sort
     * @return mixed
     */
    static public function getNotes(Customer $customer, $publicPrivate="all", $sort='desc')
    {
        if($publicPrivate=="all") {
            $a = $customer->notes()
                          ->orderBy("created_at", $sort)->get();
        }
        if($publicPrivate=="private") {
            $a = $customer->notes()
                            ->where("private", false)
                          ->orderBy("created_at", $sort)->get();
        }
        if($publicPrivate=="public") {
            $a = $customer->notes()
                            ->where("private", true)
                          ->orderBy("created_at", $sort)->get();
        }
        return $a;
    }

    /**
     * @param Customer $customer
     * @param $type Note::TYPE_SYSTEM, etc
     * @param string $publicPrivate public|private|all
     * @param string $sort
     * @return mixed
     */
    static public function getNotesByType(Customer $customer, $type, $publicPrivate="all", $sort='desc')
    {
        if($publicPrivate=="all") {
            $a = $customer->notes()->where('type', $type)->orderBy("created_at", $sort)->get();
        }
        if($publicPrivate=="private") {
            $a = $customer->notes()
                          ->where('type', $type)
                          ->where('private', true)
                          ->orderBy("created_at", $sort)->get();
        }
        if($publicPrivate=="public") {
            $a = $customer->notes()
                          ->where('type', $type)
                          ->where('private', false)
                          ->orderBy("created_at", $sort)->get();
        }
        return $a;
    }

}