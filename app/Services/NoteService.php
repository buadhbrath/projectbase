<?php

namespace App\Services;

use App\Customer;
use App\Group;
use App\Note;
use App\Traits\Service;
use App\User;

class NoteService
{
    use Service;

    /**
     * @param array $data
     * @return Note
     */
    static public function create(array $data)
    {
        $c = new Note();
        self::validateAndFill($c, $data);
        $c->save();
        return $c;
    }


    /**
     * @param Note $c
     */
    static public function delete(Note $c)
    {
        $c->delete();
    }

    /**
     * @param $id
     * @return Note
     */
    static public function getById($id)
    {
        $c = Note::find($id);
        return $c;
    }


    /**
     * @param $noteMessage
     * @param Customer $customer
     * @param $private public/private?
     * @return Note
     */
    static public function addMessageOnCustomer($noteMessage, Customer $customer, $private = false)
    {
        $n = new Note();
        $n->note = $noteMessage;
        $n->private = $private;
        $n->type = Note::TYPE_SYSTEM;
        if($customer) {
            $n->customer_id = $customer->id;
        }
        $n->save();
        return $n;
    }


    /**
     * @param $noteMessage
     * @param User $author
     * @param Customer $customer
     * @param $private public/private?
     * @return Note
     */
    static public function addMessageByUserOnCustomer($noteMessage, User $author, Customer $customer, $private = false)
    {
        $n = new Note();
        $n->note = $noteMessage;
        $n->private = $private;
        if($customer) {
            $n->customer_id = $customer->id;
        }
        if($author) {
            $n->author_id = $author->id;
            $n->type = Note::TYPE_USER;
        } else {
            $n->type = Note::TYPE_SYSTEM;
        }
        $n->save();
        return $n;
    }

}