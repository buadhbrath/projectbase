<?php

namespace App\Services;

use App\Group;
use App\Role;
use App\User;
use App\Traits\Service;
use Illuminate\Support\Facades\Hash;

class UserService
{
    use Service;

    /**
     * @param array $data
     * @return User
     */
    static public function create(array $data)
    {
        $c = new User();
        if(array_key_exists("password", $data))
        {
            self::setPassword($c, $data['password']);
        }
        self::validateAndFill($c, $data);
        $c->save();
        return $c;
    }


    /**
     * @param User $c
     */
    static public function delete(User $c)
    {
        $c->delete();
    }

    /**
     * @param $id
     * @return User
     */
    static public function getById($id)
    {
        $c = User::find($id);
        return $c;
    }


    /**
     * @param User $user
     * @param String $password
     */
    static public function setPassword(User $user, $password)
    {
        $hash = Hash::make($password);
        $user->password = $hash;
    }

    /**
     * @param User $user
     * @param array $roles array of role strings ROLE_ADMIN, ROLE_USER, etc
     *
     * either the identifiers or an array of role objects
     */
    static public function setRoles(User $user, array $roles)
    {
        $user->roles()->detach();
        foreach($roles as $role)
        {
            if(is_object($role))
            {
                $r = $role;
            } else {
                $r = Role::where('identifier', $role)->first();
            }
            $user->roles()->attach($r);
        }
    }


    /**
     * @param User $user
     * @param array $groups array of group identifiers "group1", "group2", etc
     *
     * either the identifiers or array of role objects
     */
    static public function setGroups(User $user, array $groups)
    {
        $user->groups()->detach();
        foreach($groups as $group)
        {
            if(is_object($group))
            {
                $r = $group;
            } else {
                $r = Group::where('identifier', $group)->first();
            }
            $user->groups()->attach($r);
        }
    }

}