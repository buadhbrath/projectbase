<?php
/**
 * Created by michael.
 * Date: 5/9/17 Time: 12:51 PM
 */

namespace App\Services;

use App\Location;
use App\Traits\Service;

class LocationService
{

    use Service;

    /**
     * @param array $data
     * @return Location
     */
    static public function create(array $data)
    {
        $c = new Location();
        self::validateAndFill($c, $data);
        $c->save();
        return $c;
    }


    /**
     * @param Location $c
     */
    static public function delete(Location $c)
    {
        $c->delete();
    }

    /**
     * @param $id
     * @return Location
     */
    static public function getById($id)
    {
        $c = Location::find($id);
        return $c;
    }


}