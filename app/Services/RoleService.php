<?php

namespace App\Services;

use App\Role;
use App\Traits\Service;

class RoleService
{

    use Service;

    /**
     * @param array $data
     * @return Role
     */
    static public function create(array $data)
    {
        $c = new Role();
        self::validateAndFill($c, $data);
        $c->save();
        return $c;
    }


    /**
     * @param Role $c
     */
    static public function delete(Role $c)
    {
        $c->delete();
    }

    /**
     * @param $id
     * @return Role
     */
    static public function getById($id)
    {
        $c = Role::find($id);
        return $c;
    }

    /**
     * @param $id
     * @return Role
     */
    static public function getByIdentifier($id)
    {
        $c = Role::where("identifier",$id)->get()->first();
        return $c;
    }

}