<?php

namespace App\Services;

use App\Group;
use App\Traits\Service;

class GroupService
{
    use Service;

    /**
     * @param array $data
     * @return Group
     */
    static public function create(array $data)
    {
        $c = new Group();
        self::validateAndFill($c, $data);
        $c->save();
        return $c;
    }


    /**
     * @param Group $c
     */
    static public function delete(Group $c)
    {
        $c->delete();
    }

    /**
     * @param $id
     * @return Group
     */
    static public function getById($id)
    {
        $c = Group::find($id);
        return $c;
    }


}