<?php

namespace App;

use App\Traits\Validatable;
use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    use Validatable;

    public $table = "role";

    public $_errors = [];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'identifier'
    ];


    public function users()
    {
        return $this->belongsToMany('App\User');
    }


}
