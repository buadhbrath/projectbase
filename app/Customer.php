<?php

namespace App;

use App\Traits\Validatable;
use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    use Validatable;

    protected $table = "customer";

    protected $fillable = ["first_name","last_name"];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function addresses()
    {
        return $this->hasMany('App\Address');
    }

    public function addressIds()
    {
        $ids = [];
         $x =  $this->hasMany('App\Address')->get(['id']);
         foreach($x as $i)
         {
             $ids[] = $i['id'];
         }
         return $ids;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function notes()
    {
        return $this->hasMany('App\Note');
    }


}
