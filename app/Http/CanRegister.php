<?php

namespace App\Http\Middleware;

use Closure;

class CanRegister
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(config('adminlte.register_url')!="")
        {
            return $next($request);
        }
        return redirect('/login');
    }
}
