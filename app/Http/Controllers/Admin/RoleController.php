<?php

namespace App\Http\Controllers\Admin;

use App\Group;
use App\Role;
use App\Team;
use App\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Response;

class RoleController extends Controller
{
    public function list()
    {
        return view('admin.role.list');
    }

    public function index()
    {
        $u = Role::all();
        $data = ['data'=>$u];
        return Response::json($data);
    }

    public function formshow($id)
    {
        $u = Role::find($id);
        $data = ['data'=>$u];
        return Response::json($data);
    }

    public function show($id)
    {
        $u = Role::find($id);
        $data = ['data'=>['role'=>$u]];
        return Response::json($data);
    }

    public function update(Request $request, Response $response, $id)
    {
        $g = Role::find($id);
        $g->fill($request->all());
        if(!$g->validate($request->all()))
        {
            return new JsonResponse(['errors'=>$g->errors()], 422);
        }
        $g->save();

        $data = ['role'=>$g];
        return Response::json($data);
    }

    public function store(Request $request, Response $response)
    {
        $g = new Role();
        $g->fill($request->all());
        if(!$g->validate($request->all()))
        {
            return new JsonResponse(['errors'=>$g->errors()], 422);
        }
        $g = Role::create($request->all());
        $g->save();

        $data = ['role'=>$g];
        return Response::json($data);
    }

}
