<?php

namespace App\Http\Controllers\Admin;

use App\Group;
use App\Role;
use App\Team;
use App\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Response;

class GroupController extends Controller
{
    public function list()
    {
        return view('admin.group.list');
    }

    public function index()
    {
        $u = Group::all();
        $data = ['data'=>$u];
        return Response::json($data);
    }

    public function formshow($id)
    {
        $u = Group::find($id);
        $data = ['data'=>$u];
        return Response::json($data);
    }

    public function show($id)
    {
        $u = Group::find($id);
        $data = ['data'=>['group'=>$u]];
        return Response::json($data);
    }

    public function update(Request $request, Response $response, $id)
    {
        $g = Group::find($id);
        $g->fill($request->all());
        if(!$g->validate($request->all()))
        {
            return new JsonResponse(['errors'=>$g->errors()], 422);
        }
        $g->save();

        $data = ['group'=>$g];
        return Response::json($data);
    }

    public function store(Request $request, Response $response)
    {
        $g = new Group();
        $g->fill($request->all());
        if(!$g->validate($request->all()))
        {
            return new JsonResponse(['errors'=>$g->errors()], 422);
        }
        $g = Group::create($request->all());
        $g->save();


        $data = ['group'=>$g];
        return Response::json($data);
    }

}
