<?php

namespace App\Http\Controllers\Admin;

use App\Group;
use App\Role;
use App\Team;
use App\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Response;

class IndexController extends Controller
{
    public function index()
    {
        return view('admin.index');
    }

}
