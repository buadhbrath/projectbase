<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        //
        $this->registerGates();
    }

    public function registerGates()
    {
        Gate::define('mainadmin', function ($user) {
            return $user->hasRole("ROLE_ADMIN");
        });
        Gate::define('isuser', function ($user) {
            return $user->hasRole("ROLE_USER") || $user->hasRole("ROLE_ADMIN");
        });
        Gate::define('mike',function($user){
           die('sdfsdf');
        });
        Gate::define('canregister', function ($user) {
            return (!$user->hasRole("ROLE_USER") && (config("adminlte.register_url")!=''));
        });
    }
}
