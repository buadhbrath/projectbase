<?php

namespace App;

use App\Traits\Validatable;
use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    use Validatable;

    protected $table = "location";

}
